const path = require("path");
const os = require("os");
const fs = require("fs-extra");
let devs = {};

try {
  const SHome = path.join(os.homedir(), ".s");
  devs = require(path.join(SHome, "cache/core"));
} catch (error) {
  devs = require("@serverless-devs/core");
}

const Listr = require("./listr");

const { loadComponent, installDependency, getCredential, reportComponent } =
  devs;

const fsExtra = require("fs-extra");

const currentPath = process.cwd();

async function report(context, inputs, command) {
  const credential = await getCredential(inputs, inputs.project.access);

  reportComponent(context, {
    command,
    uid: credential.AccountID,
  });
}
const getTasks = (tasks, deployPath) => {
  return Object.prototype.toString.call(tasks) === "[object Function]"
    ? tasks(deployPath) || []
    : tasks || [];
};

class BestPractice {
  constructor(context) {
    this.context = context;
  }

  async deploy(inputs, setUpTasks, restTasks) {
    const { service: serviceProps, function: functionProps } = inputs.props;
    const serviceName = serviceProps.name;
    const functionName = functionProps.name;
    const codeUri = functionProps.codeUri;
    const deployPath = path.join(currentPath, '.s', 'build', 'artifacts', serviceName, functionName);
    const codeUriPath = path.join(currentPath, codeUri);
    const fcDeployComponent = await loadComponent("devsapp/fc");
    await report(this.context, inputs, "deploy");
    const tasks = new Listr(
      [
        {
          title: "Setup deploy environment",
          task: () => {
            return new Listr(
              [
                {
                  title: "Generate publish path",
                  task: async () => await fsExtra.ensureDir(deployPath),
                },
                {
                  title: "Copy files to publish path",
                  task: async () => {
                    const tmpdir = path.join(os.tmpdir(), 'nodejs-runtime-app');
                    if (fs.existsSync(tmpdir)) {
                      await fs.removeSync(tmpdir);
                    }
                    await fs.copy(
                      codeUriPath,
                      tmpdir,
                      {
                        filter: (src) => {
                          const fileName = src.replace(path.join(codeUriPath, '/'), '');
                          const ignoreFiles = ['node_modules', '.s'];
                          if (ignoreFiles.includes(fileName)) return false;
                          return true
                        }
                      }
                    );
                    await fs.move(
                      path.join(tmpdir),
                      path.join(deployPath),
                      {
                        overwrite: true,
                      }
                    );
                  },
                },
              ].concat(getTasks(setUpTasks, deployPath))
            );
          },
        },
        {
          title: "Install deploy dependencies",
          task: async () => {
            await installDependency({ cwd: deployPath });
          },
        },
      ].concat(getTasks(restTasks, deployPath)),
      {
        // renderer: "verbose",
      }
    );
    await tasks.run();
    return await fcDeployComponent.deploy(inputs);
  }

  async build(inputs) {
    await report(this.context, inputs, "build");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.build(inputs);
  }
  async info(inputs) {
    await report(this.context, inputs, "info");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.info(inputs);
  }
  async invoke(inputs) {
    await report(this.context, inputs, "invoke");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.invoke(inputs);
  }
  async local(inputs) {
    await report(this.context, inputs, "local");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.local(inputs);
  }
  async logs(inputs) {
    await report(this.context, inputs, "logs");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.logs(inputs);
  }
  async metrics(inputs) {
    await report(this.context, inputs, "metrics");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.metrics(inputs);
  }
  async nas(inputs) {
    await report(this.context, inputs, "nas");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.nas(inputs);
  }
  async remove(inputs) {
    await report(this.context, inputs, "remove");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.remove(inputs);
  }
  async sync(inputs) {
    await report(this.context, inputs, "sync");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.sync(inputs);
  }
  async version(inputs) {
    await report(this.context, inputs, "version");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.version(inputs);
  }
  async alias(inputs) {
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.alias(inputs);
  }
  async provision(inputs) {
    await report(this.context, inputs, "provision");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.provision(inputs);
  }
  async onDemand(inputs) {
    await report(this.context, inputs, "onDemand");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.onDemand(inputs);
  }
  async layer(inputs) {
    await report(this.context, inputs, "layer");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.layer(inputs);
  }
  async proxied(inputs) {
    await report(this.context, inputs, "proxied");
    const fcDeployComponent = await loadComponent("devsapp/fc");
    return await fcDeployComponent.proxied(inputs);
  }
}

module.exports = BestPractice;
