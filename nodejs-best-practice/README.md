## nodejs-best-practice

`devsapp` Nodejs deploy 通用组件

- 统一部署流程，Nodejs生态的部署组件`交互行为`保持一致

## 使用方法
```
const core = require('@serverless-devs/core')

class Practice {
  constructor() {
    super();
  }
  async deploy(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice')
    return await BestPractice.deploy(
      inputs,
      (deployPath) => [
        {
          title: "Copy files to publish path",
          task: async () => {
            await fs.copy(
              path.join(__dirname, "template"),
              path.join(deployPath),
              {}
            );
            await fs.copy(
              path.join(currentPath, "static"),
              path.join(deployPath, "static")
            );
            await fs.copy(
              path.join(currentPath, "./nuxt.config.js"),
              path.join(deployPath, "./nuxt.config.js")
            );
          },
        },
      ],
      (deployPath) => [
        {
          title: "Run Nuxt build",
          task: async () => {
            await execa("npm", ["run", "build"]);
            await fs.move(
              path.join(currentPath, ".nuxt"),
              path.join(deployPath, ".nuxt"),
              { overwrite: true }
            );
          },
        },
      ]
    );
  }
}
```