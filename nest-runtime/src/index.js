const path = require("path");
const fs = require("fs-extra");
const execa = require("execa");
const core = require('@serverless-devs/core')
const os = require('os');

const currentPath = process.cwd();
class NestComponent {
  async deploy(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.deploy(
      inputs,
      (deployPath) => [],
      (deployPath) => [
        {
          title: "Run Nest build",
          task: async () => {
            const { function: functionProps } = inputs.props;
            const codeUri = functionProps.codeUri;
            const codeUriPath = path.join(currentPath, codeUri);
            await execa('npm', ['run', 'build'], { cwd: codeUriPath, shell: true })
            await fs.move(
              path.join(codeUriPath, "dist"),
              path.join(deployPath, "dist"),
              { overwrite: true }
            );
          },
        },
      ]
    );
  }

  async build(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.build(inputs);
  }
  async info(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.info(inputs);
  }
  async invoke(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.invoke(inputs);
  }
  async local(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.local(inputs);
  }
  async logs(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.logs(inputs);
  }
  async metrics(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.metrics(inputs);
  }
  async nas(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.nas(inputs);
  }
  async remove(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.remove(inputs);
  }
  async sync(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.sync(inputs);
  }
  async version(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.version(inputs);
  }
  async alias(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.alias(inputs);
  }
  async provision(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.provision(inputs);
  }
  async onDemand(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.onDemand(inputs);
  }
  async layer(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.layer(inputs);
  }
  async proxied(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.proxied(inputs);
  }
}

module.exports = NestComponent;
